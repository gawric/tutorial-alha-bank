package com.alfatest.alfatest.controller;

import com.alfatest.alfatest.model.RateModel;
import com.alfatest.alfatest.model.gif.GifDataModel;
import com.alfatest.alfatest.service.httpclient.IGifClientService;
import com.alfatest.alfatest.service.httpclient.IRatesClientService;
import com.alfatest.alfatest.service.httpclient.support.ICreateClient;
import com.alfatest.alfatest.service.httpclient.support.MyRequestInterceprot;
import com.alfatest.alfatest.service.servicecalendar.ICalendar;
import com.alfatest.alfatest.service.servicegif.IGif;
import com.alfatest.alfatest.service.servicerate.IServiceEqRate;
import com.alfatest.alfatest.staticVariable.settingVariable;
import feign.Feign;
import feign.Logger;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;
import feign.okhttp.OkHttpClient;
import feign.slf4j.Slf4jLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by А д м и н on 02.09.2021.
 */
@RestController
public class ExRatesController {

    @Autowired
    private ICreateClient serviceCreateClient;

    @Autowired
    private IServiceEqRate serviceRate;

    @Autowired
    private ICalendar serviceCalendar;

    @Autowired
    private IGif serviceGif;



    @GetMapping("/gif")
    public String gif() {

        String minDay = serviceCalendar.getMinusDay();

        IRatesClientService ratesClient = serviceCreateClient.createClientNoEscape(IRatesClientService.class, settingVariable.pathApiRate);
        IGifClientService gifClient = serviceCreateClient.createClientNoEscape(IGifClientService.class, settingVariable.pathApiGif);

        RateModel currentRate  =  ratesClient.findByRate(settingVariable.apiRateKey);
        RateModel yesterRate  =  ratesClient.findByRateHistory(minDay , settingVariable.apiRateKey);


        boolean isEq = serviceRate.equ(currentRate , yesterRate);
        String urlgif = serviceGif.getGifUrl(isEq ,  gifClient);



       return "<IMG SRC=\""+urlgif+"\">";
    }


}
