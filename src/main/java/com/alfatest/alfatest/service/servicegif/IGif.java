package com.alfatest.alfatest.service.servicegif;

import com.alfatest.alfatest.service.httpclient.IGifClientService;

/**
 * Created by А д м и н on 03.09.2021.
 */
public interface IGif {

    String getGifUrl(boolean isEq , IGifClientService gifClient);
}
