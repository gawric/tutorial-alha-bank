package com.alfatest.alfatest.service.servicegif;

import com.alfatest.alfatest.model.gif.GifDataModel;
import com.alfatest.alfatest.service.httpclient.IGifClientService;
import com.alfatest.alfatest.staticVariable.settingVariable;
import org.springframework.stereotype.Service;

import java.util.Random;

/**
 * Created by А д м и н on 03.09.2021.
 */
@Service
public class ServiceGifImpl implements IGif {
    @Override
    public String getGifUrl(boolean isEq , IGifClientService gifClient) {
        if(isEq)
        {
            GifDataModel gifModel  =  gifClient.findByGif(settingVariable.apiGifKey, settingVariable.rich ,settingVariable.size);
            int rnumber = getRandom(gifModel.getData().size() - 1);
            return gifModel.getData().get(rnumber).getImages().get("original").getUrl();
        }
        else
        {
            GifDataModel gifModel  =  gifClient.findByGif(settingVariable.apiGifKey , settingVariable.broke,settingVariable.size);
            int rnumber  = getRandom(gifModel.getData().size() - 1);
           return gifModel.getData().get(rnumber).getImages().get("original").getUrl();
        }
    }

    private int getRandom(int max)
    {
        int diff = max - 1;
        Random randNumber = new Random();
        return  randNumber.nextInt(diff + 1);
    }
}
