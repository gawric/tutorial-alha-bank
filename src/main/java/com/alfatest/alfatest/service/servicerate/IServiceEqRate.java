package com.alfatest.alfatest.service.servicerate;

import com.alfatest.alfatest.model.RateModel;

/**
 * Created by А д м и н on 03.09.2021.
 */
public interface IServiceEqRate {
     boolean equ(RateModel currentRate , RateModel yesterRate);
}
