package com.alfatest.alfatest.service.servicerate;

import com.alfatest.alfatest.model.RateModel;
import com.alfatest.alfatest.staticVariable.settingVariable;
import org.springframework.stereotype.Service;

/**
 * Created by А д м и н on 03.09.2021.
 */
@Service
public class ServiceEqRateImpl implements IServiceEqRate {
    @Override
    public boolean equ(RateModel currentRate , RateModel yesterRate) {

        double yester = yesterRate.getRates().get(settingVariable.rub);
        double current = currentRate.getRates().get(settingVariable.rub);
        System.out.print("Текущий курс " + yester + "\n");
        System.out.print("Вчерашний  курс  " + current + "\n");
        return isEquDay( yester ,  current);

    }

    private boolean isEquDay(double yester , double current)
    {
        //если сегодня выше вчерашнего
        if(current > yester)
        {
            return true;
        }

        return false;
    }
}
