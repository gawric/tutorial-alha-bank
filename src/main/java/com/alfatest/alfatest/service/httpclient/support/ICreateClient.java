package com.alfatest.alfatest.service.httpclient.support;

/**
 * Created by А д м и н on 03.09.2021.
 */
public interface ICreateClient {
    <T> T createClientNoEscape(Class<T> type, String uri);

    <T> T createClient(Class<T> type, String uri);
}
