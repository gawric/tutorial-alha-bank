package com.alfatest.alfatest.service.httpclient.support;

import feign.RequestInterceptor;
import feign.RequestTemplate;

/**
 * Created by А д м и н on 02.09.2021.
 */
public class MyRequestInterceprot implements RequestInterceptor {
    @Override
    public void apply(RequestTemplate requestTemplate) {
        String path = requestTemplate.queryLine();
        String path2 = requestTemplate.url().replace("%26","&");

        requestTemplate.uri(path2);
    }
}
