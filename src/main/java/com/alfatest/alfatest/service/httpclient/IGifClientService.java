package com.alfatest.alfatest.service.httpclient;

import com.alfatest.alfatest.model.gif.GifDataModel;
import feign.Param;
import feign.RequestLine;

/**
 * Created by А д м и н on 02.09.2021.
 */
public interface IGifClientService {
    //rich
    @RequestLine("GET /search?api_key={key}={name}+&limit={limit}&offset=0&rating=g&lang=en")
    GifDataModel findByGif(@Param("key") String key , @Param("name") String name ,  @Param("limit") String limit);

}
