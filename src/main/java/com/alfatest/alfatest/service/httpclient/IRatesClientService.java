package com.alfatest.alfatest.service.httpclient;

import com.alfatest.alfatest.model.RateModel;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

import java.util.List;

/**
 * Created by А д м и н on 02.09.2021.
 */
public interface IRatesClientService {
    @RequestLine("GET /latest.json?app_id={key}")
    RateModel findByRate(@Param("key") String key);

    //data - 2012-07-10
    @RequestLine("GET /historical/{data}.json?app_id={key}")
    RateModel findByRateHistory(@Param("data") String data , @Param("key") String key);

    @RequestLine("GET")
    List<RateModel> findAll();

    @RequestLine("POST")
    @Headers("Content-Type: application/json")
    void create();
}
