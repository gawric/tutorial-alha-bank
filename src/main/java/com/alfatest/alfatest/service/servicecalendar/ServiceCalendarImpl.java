package com.alfatest.alfatest.service.servicecalendar;

import org.springframework.stereotype.Service;

import java.time.LocalDate;

/**
 * Created by А д м и н on 03.09.2021.
 */
@Service
public class ServiceCalendarImpl implements ICalendar {
    @Override
    public String getMinusDay() {
        LocalDate currentDate = LocalDate.now();

        return currentDate.minusDays(1).toString();
    }
}
