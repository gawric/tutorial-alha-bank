package com.alfatest.alfatest.model.gif;

import java.util.HashMap;

/**
 * Created by А д м и н on 02.09.2021.
 */
public class GifModel {
    private String type;
    private String id;
    private String url;
    private String slug;
    private String bitly_gif_url;
    private String bitly_url;
    private String embed_url;
    private String username;
    private String source;


    private String title;
    private String rating;
    private String content_url;
    private String source_tld;
    private String source_post_url;
    private int is_sticker;
    private String import_datetime;
    private String trending_datetime;
    private HashMap<String, GifSubModelImages> images;
    private GifSubModelUser user;
    private String analytics_response_payload;
    private HashMap<String, GifSubModelAnalitic> analytics;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getBitly_gif_url() {
        return bitly_gif_url;
    }

    public void setBitly_gif_url(String bitly_gif_url) {
        this.bitly_gif_url = bitly_gif_url;
    }

    public String getBitly_url() {
        return bitly_url;
    }

    public void setBitly_url(String bitly_url) {
        this.bitly_url = bitly_url;
    }

    public String getEmbed_url() {
        return embed_url;
    }

    public void setEmbed_url(String embed_url) {
        this.embed_url = embed_url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getContent_url() {
        return content_url;
    }

    public void setContent_url(String content_url) {
        this.content_url = content_url;
    }

    public String getSource_tld() {
        return source_tld;
    }

    public void setSource_tld(String source_tld) {
        this.source_tld = source_tld;
    }

    public String getSource_post_url() {
        return source_post_url;
    }

    public void setSource_post_url(String source_post_url) {
        this.source_post_url = source_post_url;
    }

    public int getIs_sticker() {
        return is_sticker;
    }

    public void setIs_sticker(int is_sticker) {
        this.is_sticker = is_sticker;
    }

    public String getImport_datetime() {
        return import_datetime;
    }

    public void setImport_datetime(String import_datetime) {
        this.import_datetime = import_datetime;
    }

    public String getTrending_datetime() {
        return trending_datetime;
    }

    public void setTrending_datetime(String trending_datetime) {
        this.trending_datetime = trending_datetime;
    }

    public HashMap<String, GifSubModelImages> getImages() {
        return images;
    }

    public void setImages(HashMap<String, GifSubModelImages> images) {
        this.images = images;
    }

    public GifSubModelUser getUser() {
       return user;
    }

    public void setUser(GifSubModelUser user) {
        this.user = user;
    }

    public String getAnalytics_response_payload() {
        return analytics_response_payload;
    }

    public void setAnalytics_response_payload(String analytics_response_payload) {
        this.analytics_response_payload = analytics_response_payload;
    }

    public HashMap<String, GifSubModelAnalitic> getAnalytics() {
        return analytics;
   }

    public void setAnalytics(HashMap<String, GifSubModelAnalitic> analytics) {
        this.analytics = analytics;
    }

}
