package com.alfatest.alfatest.model.gif;

/**
 * Created by А д м и н on 02.09.2021.
 */
public class GifPaginationModel {
    private int total_count;
    private int count;
    private int offset;

    public int getTotal_count() {
        return total_count;
    }

    public void setTotal_count(int total_count) {
        this.total_count = total_count;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }
}
