package com.alfatest.alfatest.model.gif;

import java.util.List;

/**
 * Created by А д м и н on 02.09.2021.
 */
public class GifDataModel {

    private List<GifModel> data;
    private GifPaginationModel pagination;
    private GifMetaModel meta;

    public List<GifModel> getData() {
        return data;
    }

    public void setData(List<GifModel> data) {
        this.data = data;
    }


    public GifPaginationModel getPagination() {
        return pagination;
    }

    public void setPagination(GifPaginationModel pagination) {
        this.pagination = pagination;
    }

    public GifMetaModel getMeta() {
        return meta;
    }

    public void setMeta(GifMetaModel meta) {
        this.meta = meta;
    }
}
