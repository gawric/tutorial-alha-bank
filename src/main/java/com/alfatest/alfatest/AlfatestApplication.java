package com.alfatest.alfatest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlfatestApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlfatestApplication.class, args);
	}

}
