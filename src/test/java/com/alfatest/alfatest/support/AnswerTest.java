package com.alfatest.alfatest.support;

/**
 * Created by А д м и н on 08.09.2021.
 */
public class AnswerTest
{
    public String getResponceJsonRate()
    {
        return "{\n" +
                "  \"disclaimer\": \"Usage subject to terms: https://openexchangerates.org/terms\",\n" +
                "  \"license\": \"https://openexchangerates.org/license\",\n" +
                "  \"timestamp\": 1631012400,\n" +
                "  \"base\": \"USD\",\n" +
                "  \"rates\": {\n" +
                "    \"AED\": 3.672989,\n" +
                "    \"AFN\": 86.890566,\n" +
                "    \"ALL\": 102.576868,\n" +
                "    \"AMD\": 493.432427,\n" +
                "    \"ANG\": 1.795019,\n" +
                "    \"AOA\": 633,\n" +
                "    \"ARS\": 97.9512,\n" +
                "    \"AUD\": 1.351661,\n" +
                "    \"AWG\": 1.8005,\n" +
                "    \"AZN\": 1.700805,\n" +
                "    \"BAM\": 1.648368,\n" +
                "    \"BBD\": 2,\n" +
                "    \"BDT\": 85.195363,\n" +
                "    \"BGN\": 1.647886,\n" +
                "    \"BHD\": 0.376912,\n" +
                "    \"BIF\": 1985.071347,\n" +
                "    \"BMD\": 1,\n" +
                "    \"BND\": 1.342463,\n" +
                "    \"BOB\": 6.894759,\n" +
                "    \"BRL\": 5.1712,\n" +
                "    \"BSD\": 1,\n" +
                "    \"BTC\": 0.000019536434,\n" +
                "    \"BTN\": 73.036961,\n" +
                "    \"BWP\": 10.95861,\n" +
                "    \"BYN\": 2.518531,\n" +
                "    \"BZD\": 2.015659,\n" +
                "    \"CAD\": 1.258346,\n" +
                "    \"CDF\": 1985.946372,\n" +
                "    \"CHF\": 0.913833,\n" +
                "    \"CLF\": 0.02794,\n" +
                "    \"CLP\": 773.15,\n" +
                "    \"CNH\": 6.459985,\n" +
                "    \"CNY\": 6.4643,\n" +
                "    \"COP\": 3796.973842,\n" +
                "    \"CRC\": 624.24331,\n" +
                "    \"CUC\": 1,\n" +
                "    \"CUP\": 25.75,\n" +
                "    \"CVE\": 93.100946,\n" +
                "    \"CZK\": 21.38945,\n" +
                "    \"DJF\": 178.015636,\n" +
                "    \"DKK\": 6.26227,\n" +
                "    \"DOP\": 56.821605,\n" +
                "    \"DZD\": 135.913646,\n" +
                "    \"EGP\": 15.7342,\n" +
                "    \"ERN\": 15.004907,\n" +
                "    \"ETB\": 45.598197,\n" +
                "    \"EUR\": 0.842134,\n" +
                "    \"FJD\": 2.0696,\n" +
                "    \"FKP\": 0.725026,\n" +
                "    \"GBP\": 0.725026,\n" +
                "    \"GEL\": 3.104833,\n" +
                "    \"GGP\": 0.725026,\n" +
                "    \"GHS\": 6.059712,\n" +
                "    \"GIP\": 0.725026,\n" +
                "    \"GMD\": 51.18,\n" +
                "    \"GNF\": 9788.136372,\n" +
                "    \"GTQ\": 7.741275,\n" +
                "    \"GYD\": 209.210234,\n" +
                "    \"HKD\": 7.77335,\n" +
                "    \"HNL\": 23.964769,\n" +
                "    \"HRK\": 6.2991,\n" +
                "    \"HTG\": 96.523237,\n" +
                "    \"HUF\": 293.102725,\n" +
                "    \"IDR\": 14261.25,\n" +
                "    \"ILS\": 3.204482,\n" +
                "    \"IMP\": 0.725026,\n" +
                "    \"INR\": 73.435947,\n" +
                "    \"IQD\": 1458.948889,\n" +
                "    \"IRR\": 42204.079952,\n" +
                "    \"ISK\": 127,\n" +
                "    \"JEP\": 0.725026,\n" +
                "    \"JMD\": 150.742976,\n" +
                "    \"JOD\": 0.709,\n" +
                "    \"JPY\": 109.9405,\n" +
                "    \"KES\": 109.95,\n" +
                "    \"KGS\": 84.733301,\n" +
                "    \"KHR\": 4081.702494,\n" +
                "    \"KMF\": 418.999739,\n" +
                "    \"KPW\": 900,\n" +
                "    \"KRW\": 1162.992203,\n" +
                "    \"KWD\": 0.300635,\n" +
                "    \"KYD\": 0.833411,\n" +
                "    \"KZT\": 425.233182,\n" +
                "    \"LAK\": 9580.409844,\n" +
                "    \"LBP\": 1511.998329,\n" +
                "    \"LKR\": 200.240675,\n" +
                "    \"LRD\": 171.850008,\n" +
                "    \"LSL\": 14.259708,\n" +
                "    \"LYD\": 4.505763,\n" +
                "    \"MAD\": 8.923959,\n" +
                "    \"MDL\": 17.594877,\n" +
                "    \"MGA\": 3918.295292,\n" +
                "    \"MKD\": 51.911892,\n" +
                "    \"MMK\": 1645.8774,\n" +
                "    \"MNT\": 2843.125572,\n" +
                "    \"MOP\": 8.006658,\n" +
                "    \"MRO\": 356.999828,\n" +
                "    \"MRU\": 36.258823,\n" +
                "    \"MUR\": 42.550001,\n" +
                "    \"MVR\": 15.450031,\n" +
                "    \"MWK\": 812.476546,\n" +
                "    \"MXN\": 19.9471,\n" +
                "    \"MYR\": 4.1555,\n" +
                "    \"MZN\": 63.775,\n" +
                "    \"NAD\": 14.92,\n" +
                "    \"NGN\": 411.25,\n" +
                "    \"NIO\": 35.113908,\n" +
                "    \"NOK\": 8.665165,\n" +
                "    \"NPR\": 116.857475,\n" +
                "    \"NZD\": 1.403408,\n" +
                "    \"OMR\": 0.385002,\n" +
                "    \"PAB\": 1,\n" +
                "    \"PEN\": 4.095625,\n" +
                "    \"PGK\": 3.55594,\n" +
                "    \"PHP\": 50.344647,\n" +
                "    \"PKR\": 167.092234,\n" +
                "    \"PLN\": 3.806367,\n" +
                "    \"PYG\": 6919.288141,\n" +
                "    \"QAR\": 3.641819,\n" +
                "    \"RON\": 4.1683,\n" +
                "    \"RSD\": 99.096079,\n" +
                "    \"RUB\": 73.2579,\n" +
                "    \"RWF\": 1009.432309,\n" +
                "    \"SAR\": 3.750658,\n" +
                "    \"SBD\": 8.061328,\n" +
                "    \"SCR\": 12.871817,\n" +
                "    \"SDG\": 443.5,\n" +
                "    \"SEK\": 8.554123,\n" +
                "    \"SGD\": 1.344538,\n" +
                "    \"SHP\": 0.725026,\n" +
                "    \"SLL\": 10317.350152,\n" +
                "    \"SOS\": 578.503136,\n" +
                "    \"SRD\": 21.3705,\n" +
                "    \"SSP\": 130.26,\n" +
                "    \"STD\": 20747.790504,\n" +
                "    \"STN\": 21.2,\n" +
                "    \"SVC\": 8.750032,\n" +
                "    \"SYP\": 1257.697991,\n" +
                "    \"SZL\": 14.256927,\n" +
                "    \"THB\": 32.605,\n" +
                "    \"TJS\": 11.338994,\n" +
                "    \"TMT\": 3.51,\n" +
                "    \"TND\": 2.787892,\n" +
                "    \"TOP\": 2.241265,\n" +
                "    \"TRY\": 8.332702,\n" +
                "    \"TTD\": 6.79084,\n" +
                "    \"TWD\": 27.6325,\n" +
                "    \"TZS\": 2319,\n" +
                "    \"UAH\": 26.77196,\n" +
                "    \"UGX\": 3522.896453,\n" +
                "    \"USD\": 1,\n" +
                "    \"UYU\": 42.595495,\n" +
                "    \"UZS\": 10684.802898,\n" +
                "    \"VES\": 4105461.245,\n" +
                "    \"VND\": 22745.239456,\n" +
                "    \"VUV\": 111.383463,\n" +
                "    \"WST\": 2.560467,\n" +
                "    \"XAF\": 552.40345,\n" +
                "    \"XAG\": 0.04125413,\n" +
                "    \"XAU\": 0.00055237,\n" +
                "    \"XCD\": 2.70255,\n" +
                "    \"XDR\": 0.704365,\n" +
                "    \"XOF\": 552.40345,\n" +
                "    \"XPD\": 0.00041701,\n" +
                "    \"XPF\": 100.493273,\n" +
                "    \"XPT\": 0.00098817,\n" +
                "    \"YER\": 250.849982,\n" +
                "    \"ZAR\": 14.337088,\n" +
                "    \"ZMW\": 16.105855,\n" +
                "    \"ZWL\": 322\n" +
                "  }\n" +
                "}";
    }

    public String getResponceJsonGif()
    {
        return "{\"data\":[{\"type\":\"gif\",\"id\":\"ZGH8VtTZMmnwzsYYMf\",\"url\":\"https://giphy.com/gifs/mostexpensivest-viceland-2-chainz-most-expensivest-ZGH8VtTZMmnwzsYYMf\",\"slug\":\"mostexpensivest-viceland-2-chainz-most-expensivest-ZGH8VtTZMmnwzsYYMf\",\"bitly_gif_url\":\"https://gph.is/g/E3lAKJx\",\"bitly_url\":\"https://gph.is/g/E3lAKJx\",\"embed_url\":\"https://giphy.com/embed/ZGH8VtTZMmnwzsYYMf\",\"username\":\"mostexpensivest\",\"source\":\"\",\"title\":\"2 Chainz Pockets GIF by MOST EXPENSIVEST\",\"rating\":\"g\",\"content_url\":\"\",\"source_tld\":\"\",\"source_post_url\":\"\",\"is_sticker\":0,\"import_datetime\":\"2019-07-29 14:55:52\",\"trending_datetime\":\"0000-00-00 00:00:00\",\"images\":{\"original\":{\"height\":\"270\",\"width\":\"480\",\"size\":\"935092\",\"url\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/giphy.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=giphy.gif&ct=g\",\"mp4_size\":\"178155\",\"mp4\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/giphy.mp4?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=giphy.mp4&ct=g\",\"webp_size\":\"312820\",\"webp\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/giphy.webp?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=giphy.webp&ct=g\",\"frames\":\"30\",\"hash\":\"02da6ac1ff548dfc0c9bc8d5f914a8d2\"},\"downsized\":{\"height\":\"270\",\"width\":\"480\",\"size\":\"935092\",\"url\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/giphy.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=giphy.gif&ct=g\"},\"downsized_large\":{\"height\":\"270\",\"width\":\"480\",\"size\":\"935092\",\"url\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/giphy.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=giphy.gif&ct=g\"},\"downsized_medium\":{\"height\":\"270\",\"width\":\"480\",\"size\":\"935092\",\"url\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/giphy.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=giphy.gif&ct=g\"},\"downsized_small\":{\"height\":\"270\",\"width\":\"480\",\"mp4_size\":\"178155\",\"mp4\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/giphy-downsized-small.mp4?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=giphy-downsized-small.mp4&ct=g\"},\"downsized_still\":{\"height\":\"270\",\"width\":\"480\",\"size\":\"935092\",\"url\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/giphy_s.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=giphy_s.gif&ct=g\"},\"fixed_height\":{\"height\":\"200\",\"width\":\"356\",\"size\":\"497906\",\"url\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/200.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=200.gif&ct=g\",\"mp4_size\":\"109218\",\"mp4\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/200.mp4?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=200.mp4&ct=g\",\"webp_size\":\"223426\",\"webp\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/200.webp?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=200.webp&ct=g\"},\"fixed_height_downsampled\":{\"height\":\"200\",\"width\":\"356\",\"size\":\"139711\",\"url\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/200_d.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=200_d.gif&ct=g\",\"webp_size\":\"100388\",\"webp\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/200_d.webp?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=200_d.webp&ct=g\"},\"fixed_height_small\":{\"height\":\"100\",\"width\":\"178\",\"size\":\"184568\",\"url\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/100.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=100.gif&ct=g\",\"mp4_size\":\"37337\",\"mp4\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/100.mp4?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=100.mp4&ct=g\",\"webp_size\":\"87124\",\"webp\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/100.webp?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=100.webp&ct=g\"},\"fixed_height_small_still\":{\"height\":\"100\",\"width\":\"178\",\"size\":\"8268\",\"url\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/100_s.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=100_s.gif&ct=g\"},\"fixed_height_still\":{\"height\":\"200\",\"width\":\"356\",\"size\":\"20352\",\"url\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/200_s.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=200_s.gif&ct=g\"},\"fixed_width\":{\"height\":\"113\",\"width\":\"200\",\"size\":\"198960\",\"url\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/200w.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=200w.gif&ct=g\",\"mp4_size\":\"42955\",\"mp4\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/200w.mp4?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=200w.mp4&ct=g\",\"webp_size\":\"99822\",\"webp\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/200w.webp?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=200w.webp&ct=g\"},\"fixed_width_downsampled\":{\"height\":\"113\",\"width\":\"200\",\"size\":\"56713\",\"url\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/200w_d.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=200w_d.gif&ct=g\",\"webp_size\":\"38148\",\"webp\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/200w_d.webp?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=200w_d.webp&ct=g\"},\"fixed_width_small\":{\"height\":\"57\",\"width\":\"100\",\"size\":\"73425\",\"url\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/100w.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=100w.gif&ct=g\",\"mp4_size\":\"14565\",\"mp4\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/100w.mp4?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=100w.mp4&ct=g\",\"webp_size\":\"37150\",\"webp\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/100w.webp?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=100w.webp&ct=g\"},\"fixed_width_small_still\":{\"height\":\"57\",\"width\":\"100\",\"size\":\"3483\",\"url\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/100w_s.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=100w_s.gif&ct=g\"},\"fixed_width_still\":{\"height\":\"113\",\"width\":\"200\",\"size\":\"10929\",\"url\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/200w_s.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=200w_s.gif&ct=g\"},\"looping\":{\"mp4_size\":\"1264801\",\"mp4\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/giphy-loop.mp4?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=giphy-loop.mp4&ct=g\"},\"original_still\":{\"height\":\"270\",\"width\":\"480\",\"size\":\"43878\",\"url\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/giphy_s.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=giphy_s.gif&ct=g\"},\"original_mp4\":{\"height\":\"270\",\"width\":\"480\",\"mp4_size\":\"178155\",\"mp4\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/giphy.mp4?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=giphy.mp4&ct=g\"},\"preview\":{\"height\":\"204\",\"width\":\"362\",\"mp4_size\":\"30658\",\"mp4\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/giphy-preview.mp4?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=giphy-preview.mp4&ct=g\"},\"preview_gif\":{\"height\":\"58\",\"width\":\"103\",\"size\":\"46753\",\"url\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/giphy-preview.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=giphy-preview.gif&ct=g\"},\"preview_webp\":{\"height\":\"130\",\"width\":\"232\",\"size\":\"46478\",\"url\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/giphy-preview.webp?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=giphy-preview.webp&ct=g\"},\"hd\":{\"height\":\"1080\",\"width\":\"1920\",\"mp4_size\":\"2308804\",\"mp4\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/giphy-hd.mp4?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=giphy-hd.mp4&ct=g\"},\"480w_still\":{\"height\":\"270\",\"width\":\"480\",\"size\":\"935092\",\"url\":\"https://media0.giphy.com/media/ZGH8VtTZMmnwzsYYMf/480w_s.jpg?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=480w_s.jpg&ct=g\"}},\"user\":{\"avatar_url\":\"https://media1.giphy.com/avatars/mostexpensivest/cygvi0t3e64D.jpg\",\"banner_image\":\"https://media1.giphy.com/headers/mostexpensivest/oPtwCPCUQ2Uj.jpg\",\"banner_url\":\"https://media1.giphy.com/headers/mostexpensivest/oPtwCPCUQ2Uj.jpg\",\"profile_url\":\"https://giphy.com/mostexpensivest/\",\"username\":\"mostexpensivest\",\"display_name\":\"MOST EXPENSIVEST\",\"description\":\"Better get your stacks in line, Jack, because 2 Chainz is back for an extravagant third season. The future of meat, technology, sex toys, and more 0’s than you can count make this the most globally responsible and financially foolish season yet.\",\"instagram_url\":\"\",\"website_url\":\"https://www.viceland.com/en_us/show/most-expensivest\",\"is_verified\":true},\"analytics_response_payload\":\"e=Z2lmX2lkPVpHSDhWdFRaTW1ud3pzWVlNZiZldmVudF90eXBlPUdJRl9TRUFSQ0gmY2lkPTU4MWNjY2NkOWhzZnZ5Ynh6d3F0ZDY3NWZoaXpyazVnYjcydXNxMmowbmoxdG03YSZjdD1naWY\",\"analytics\":{\"onload\":{\"url\":\"https://giphy-analytics.giphy.com/v2/pingback_simple?analytics_response_payload=e%3DZ2lmX2lkPVpHSDhWdFRaTW1ud3pzWVlNZiZldmVudF90eXBlPUdJRl9TRUFSQ0gmY2lkPTU4MWNjY2NkOWhzZnZ5Ynh6d3F0ZDY3NWZoaXpyazVnYjcydXNxMmowbmoxdG03YSZjdD1naWY&action_type=SEEN\"},\"onclick\":{\"url\":\"https://giphy-analytics.giphy.com/v2/pingback_simple?analytics_response_payload=e%3DZ2lmX2lkPVpHSDhWdFRaTW1ud3pzWVlNZiZldmVudF90eXBlPUdJRl9TRUFSQ0gmY2lkPTU4MWNjY2NkOWhzZnZ5Ynh6d3F0ZDY3NWZoaXpyazVnYjcydXNxMmowbmoxdG03YSZjdD1naWY&action_type=CLICK\"},\"onsent\":{\"url\":\"https://giphy-analytics.giphy.com/v2/pingback_simple?analytics_response_payload=e%3DZ2lmX2lkPVpHSDhWdFRaTW1ud3pzWVlNZiZldmVudF90eXBlPUdJRl9TRUFSQ0gmY2lkPTU4MWNjY2NkOWhzZnZ5Ynh6d3F0ZDY3NWZoaXpyazVnYjcydXNxMmowbmoxdG03YSZjdD1naWY&action_type=SENT\"}}},{\"type\":\"gif\",\"id\":\"TKvHkcnbtGgKxQwlqu\",\"url\":\"https://giphy.com/gifs/snl-saturday-night-live-season-45-TKvHkcnbtGgKxQwlqu\",\"slug\":\"snl-saturday-night-live-season-45-TKvHkcnbtGgKxQwlqu\",\"bitly_gif_url\":\"https://gph.is/g/E1WWNJD\",\"bitly_url\":\"https://gph.is/g/E1WWNJD\",\"embed_url\":\"https://giphy.com/embed/TKvHkcnbtGgKxQwlqu\",\"username\":\"snl\",\"source\":\"https://www.nbc.com/saturday-night-live\",\"title\":\"Episode 12 Snl GIF by Saturday Night Live\",\"rating\":\"g\",\"content_url\":\"\",\"source_tld\":\"www.nbc.com\",\"source_post_url\":\"https://www.nbc.com/saturday-night-live\",\"is_sticker\":0,\"import_datetime\":\"2020-02-02 06:01:44\",\"trending_datetime\":\"2021-01-30 18:45:10\",\"images\":{\"original\":{\"height\":\"276\",\"width\":\"480\",\"size\":\"519110\",\"url\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/giphy.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=giphy.gif&ct=g\",\"mp4_size\":\"71157\",\"mp4\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/giphy.mp4?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=giphy.mp4&ct=g\",\"webp_size\":\"180864\",\"webp\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/giphy.webp?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=giphy.webp&ct=g\",\"frames\":\"13\",\"hash\":\"909499d152745f9b5a8312fb4fd56996\"},\"downsized\":{\"height\":\"276\",\"width\":\"480\",\"size\":\"519110\",\"url\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/giphy.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=giphy.gif&ct=g\"},\"downsized_large\":{\"height\":\"276\",\"width\":\"480\",\"size\":\"519110\",\"url\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/giphy.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=giphy.gif&ct=g\"},\"downsized_medium\":{\"height\":\"276\",\"width\":\"480\",\"size\":\"519110\",\"url\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/giphy.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=giphy.gif&ct=g\"},\"downsized_small\":{\"height\":\"276\",\"width\":\"480\",\"mp4_size\":\"79604\",\"mp4\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/giphy-downsized-small.mp4?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=giphy-downsized-small.mp4&ct=g\"},\"downsized_still\":{\"height\":\"276\",\"width\":\"480\",\"size\":\"519110\",\"url\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/giphy_s.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=giphy_s.gif&ct=g\"},\"fixed_height\":{\"height\":\"200\",\"width\":\"348\",\"size\":\"239856\",\"url\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/200.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=200.gif&ct=g\",\"mp4_size\":\"38575\",\"mp4\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/200.mp4?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=200.mp4&ct=g\",\"webp_size\":\"118200\",\"webp\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/200.webp?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=200.webp&ct=g\"},\"fixed_height_downsampled\":{\"height\":\"200\",\"width\":\"348\",\"size\":\"119133\",\"url\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/200_d.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=200_d.gif&ct=g\",\"webp_size\":\"76700\",\"webp\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/200_d.webp?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=200_d.webp&ct=g\"},\"fixed_height_small\":{\"height\":\"100\",\"width\":\"174\",\"size\":\"80433\",\"url\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/100.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=100.gif&ct=g\",\"mp4_size\":\"14530\",\"mp4\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/100.mp4?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=100.mp4&ct=g\",\"webp_size\":\"44022\",\"webp\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/100.webp?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=100.webp&ct=g\"},\"fixed_height_small_still\":{\"height\":\"100\",\"width\":\"174\",\"size\":\"7556\",\"url\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/100_s.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=100_s.gif&ct=g\"},\"fixed_height_still\":{\"height\":\"200\",\"width\":\"348\",\"size\":\"21182\",\"url\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/200_s.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=200_s.gif&ct=g\"},\"fixed_width\":{\"height\":\"115\",\"width\":\"200\",\"size\":\"96471\",\"url\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/200w.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=200w.gif&ct=g\",\"mp4_size\":\"17561\",\"mp4\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/200w.mp4?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=200w.mp4&ct=g\",\"webp_size\":\"54204\",\"webp\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/200w.webp?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=200w.webp&ct=g\"},\"fixed_width_downsampled\":{\"height\":\"115\",\"width\":\"200\",\"size\":\"47655\",\"url\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/200w_d.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=200w_d.gif&ct=g\",\"webp_size\":\"32264\",\"webp\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/200w_d.webp?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=200w_d.webp&ct=g\"},\"fixed_width_small\":{\"height\":\"58\",\"width\":\"100\",\"size\":\"33136\",\"url\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/100w.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=100w.gif&ct=g\",\"mp4_size\":\"6764\",\"mp4\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/100w.mp4?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=100w.mp4&ct=g\",\"webp_size\":\"20306\",\"webp\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/100w.webp?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=100w.webp&ct=g\"},\"fixed_width_small_still\":{\"height\":\"58\",\"width\":\"100\",\"size\":\"3540\",\"url\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/100w_s.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=100w_s.gif&ct=g\"},\"fixed_width_still\":{\"height\":\"115\",\"width\":\"200\",\"size\":\"9829\",\"url\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/200w_s.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=200w_s.gif&ct=g\"},\"looping\":{\"mp4_size\":\"1309080\",\"mp4\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/giphy-loop.mp4?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=giphy-loop.mp4&ct=g\"},\"original_still\":{\"height\":\"276\",\"width\":\"480\",\"size\":\"70163\",\"url\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/giphy_s.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=giphy_s.gif&ct=g\"},\"original_mp4\":{\"height\":\"274\",\"width\":\"480\",\"mp4_size\":\"71157\",\"mp4\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/giphy.mp4?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=giphy.mp4&ct=g\"},\"preview\":{\"height\":\"218\",\"width\":\"379\",\"mp4_size\":\"24338\",\"mp4\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/giphy-preview.mp4?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=giphy-preview.mp4&ct=g\"},\"preview_gif\":{\"height\":\"60\",\"width\":\"104\",\"size\":\"48058\",\"url\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/giphy-preview.gif?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=giphy-preview.gif&ct=g\"},\"preview_webp\":{\"height\":\"124\",\"width\":\"216\",\"size\":\"45586\",\"url\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/giphy-preview.webp?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=giphy-preview.webp&ct=g\"},\"480w_still\":{\"height\":\"276\",\"width\":\"480\",\"size\":\"519110\",\"url\":\"https://media4.giphy.com/media/TKvHkcnbtGgKxQwlqu/480w_s.jpg?cid=581ccccd9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a&rid=480w_s.jpg&ct=g\"}},\"user\":{\"avatar_url\":\"https://media1.giphy.com/channel_assets/snl/FNmjSGabYyy5.jpg\",\"banner_image\":\"\",\"banner_url\":\"\",\"profile_url\":\"https://giphy.com/snl/\",\"username\":\"snl\",\"display_name\":\"Saturday Night Live\",\"description\":\"The official GIPHY channel for Saturday Night Live. Saturdays at 11:30/10:30c! #SNL\",\"instagram_url\":\"https://instagram.com/nbcsnl\",\"website_url\":\"http://www.nbc.com/saturday-night-live\",\"is_verified\":true},\"analytics_response_payload\":\"e=Z2lmX2lkPVRLdkhrY25idEdnS3hRd2xxdSZldmVudF90eXBlPUdJRl9TRUFSQ0gmY2lkPTU4MWNjY2NkOWhzZnZ5Ynh6d3F0ZDY3NWZoaXpyazVnYjcydXNxMmowbmoxdG03YSZjdD1naWY\",\"analytics\":{\"onload\":{\"url\":\"https://giphy-analytics.giphy.com/v2/pingback_simple?analytics_response_payload=e%3DZ2lmX2lkPVRLdkhrY25idEdnS3hRd2xxdSZldmVudF90eXBlPUdJRl9TRUFSQ0gmY2lkPTU4MWNjY2NkOWhzZnZ5Ynh6d3F0ZDY3NWZoaXpyazVnYjcydXNxMmowbmoxdG03YSZjdD1naWY&action_type=SEEN\"},\"onclick\":{\"url\":\"https://giphy-analytics.giphy.com/v2/pingback_simple?analytics_response_payload=e%3DZ2lmX2lkPVRLdkhrY25idEdnS3hRd2xxdSZldmVudF90eXBlPUdJRl9TRUFSQ0gmY2lkPTU4MWNjY2NkOWhzZnZ5Ynh6d3F0ZDY3NWZoaXpyazVnYjcydXNxMmowbmoxdG03YSZjdD1naWY&action_type=CLICK\"},\"onsent\":{\"url\":\"https://giphy-analytics.giphy.com/v2/pingback_simple?analytics_response_payload=e%3DZ2lmX2lkPVRLdkhrY25idEdnS3hRd2xxdSZldmVudF90eXBlPUdJRl9TRUFSQ0gmY2lkPTU4MWNjY2NkOWhzZnZ5Ynh6d3F0ZDY3NWZoaXpyazVnYjcydXNxMmowbmoxdG03YSZjdD1naWY&action_type=SENT\"}}}],\"pagination\":{\"total_count\":3476,\"count\":2,\"offset\":0},\"meta\":{\"status\":200,\"msg\":\"OK\",\"response_id\":\"9hsfvybxzwqtd675fhizrk5gb72usq2j0nj1tm7a\"}}";
    }
}
