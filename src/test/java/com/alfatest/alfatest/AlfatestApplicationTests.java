package com.alfatest.alfatest;

import com.alfatest.alfatest.model.RateModel;
import com.alfatest.alfatest.service.servicerate.IServiceEqRate;
import com.alfatest.alfatest.service.servicerate.ServiceEqRateImpl;
import com.alfatest.alfatest.staticVariable.settingVariable;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.github.tomakehurst.wiremock.WireMockServer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;


@RunWith(MockitoJUnitRunner.class)
public class AlfatestApplicationTests {


	private IServiceEqRate serviceRate = new ServiceEqRateImpl();


	@InjectMocks
	RateModel currentRate;

	@InjectMocks
	RateModel yesterRate;

//mnon
	@Test
	public void equalsCurrent72ToYester73() {
		setCurrentRate(72.4);
		setYesterRate(73.4);
		boolean isEq = serviceRate.equ(currentRate , yesterRate);
		assertEquals(false, isEq);
	}

	@Test
	public void equalsCurrent73ToYester72() {
		setCurrentRate(73.4);
		setYesterRate(72.4);
		boolean isEq = serviceRate.equ(currentRate , yesterRate);
		assertEquals(true, isEq);
	}

	private RateModel setCurrentRate(double rates)
	{

		currentRate.setRates(mock(HashMap.class));
		when(currentRate.getRates().get(settingVariable.rub)).thenReturn(rates);

		return currentRate;

	}

	private RateModel setYesterRate(double rates)
	{

		yesterRate.setRates(mock(HashMap.class));
		when(yesterRate.getRates().get(settingVariable.rub)).thenReturn(rates);

		return yesterRate;

	}

}
