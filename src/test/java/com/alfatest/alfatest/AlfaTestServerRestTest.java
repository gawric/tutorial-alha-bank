package com.alfatest.alfatest;

import com.alfatest.alfatest.model.RateModel;
import com.alfatest.alfatest.service.httpclient.IGifClientService;
import com.alfatest.alfatest.service.httpclient.IRatesClientService;
import com.alfatest.alfatest.service.httpclient.support.ICreateClient;
import com.alfatest.alfatest.service.httpclient.support.ServiceCreateClientImpl;
import com.alfatest.alfatest.service.servicegif.IGif;
import com.alfatest.alfatest.service.servicegif.ServiceGifImpl;
import com.alfatest.alfatest.staticVariable.settingVariable;
import com.alfatest.alfatest.support.AnswerTest;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;


import static com.github.tomakehurst.wiremock.client.WireMock.*;

import static org.junit.Assert.assertNotNull;

/**
 * Created by А д м и н on 07.09.2021.13123
 */

public class AlfaTestServerRestTest {


    private ICreateClient serviceCreateClient = new ServiceCreateClientImpl();
    private AnswerTest answer = new AnswerTest();
    private IGif serviceGif = new ServiceGifImpl();

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(8080);

    @Before
    public void init() {



        stubFor(get(urlEqualTo("/api/latest.json?app_id=151045b4ed034bd7a8d4882da79bb3ff"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(answer.getResponceJsonRate())));


        stubFor(get(urlMatching("/v1/gifs/.*"))

                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(answer.getResponceJsonGif())));
    }

    @Test
    public void requestRatesApi() {
        IRatesClientService ratesClient = serviceCreateClient.createClientNoEscape(IRatesClientService.class, settingVariable.pathApiRateTest);
        RateModel currentRate  =  ratesClient.findByRate(settingVariable.apiRateKey);
        assertNotNull(currentRate);
    }

    @Test
    public void requestGifApi() {
        IGifClientService gifClient = serviceCreateClient.createClientNoEscape(IGifClientService.class, settingVariable.pathApiGifTest);
        String urlgif = serviceGif.getGifUrl(true ,  gifClient);
        assertNotNull(urlgif);
    }



}
